package controllers;

import infra.SpringContextUtils;
import models.Person;
import models.PersonRepository;
import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {

	//@Security.Authenticated(UserAuthenticator.class)
	public Result index() {
		PersonRepository personRepository = SpringContextUtils.getSpringBean(PersonRepository.class);
		final Person person = new Person();
		person.firstname = "Bruce";
		person.surname = "Smith";

		final Person savedPerson = personRepository.save(person);

		final Person retrievedPerson = personRepository.findOne(savedPerson.id);

		// Deliver the index page with a message showing the id that was
		// generated
		return ok(views.html.index.render("Found id: " + retrievedPerson.id
				+ " of person/people"));
	}

}
