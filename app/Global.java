import infra.SpringContextUtils;
import play.Application;
import play.GlobalSettings;

/**
 * Application wide behaviour. We establish a Spring application context for the dependency injection system and
 * configure Spring Data.
 */
public class Global extends GlobalSettings {

    /**
     * Declare the application context to be used - a Java annotation based application context requiring no XML.
     */
    final private SpringContextUtils springContextUtils = new SpringContextUtils();
    
    /**
     * Sync the context lifecycle with Play's.
     */
    @Override
    public void onStart(final Application app) {
        super.onStart(app);
        springContextUtils.registerContext();
    }

    /**
     * Sync the context lifecycle with Play's.
     */
    @Override
    public void onStop(final Application app) {
    	springContextUtils.deregisterContext();
        super.onStop(app); 
    }    
}
