package infra;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import play.Logger;


/**
 * Used to scan spring beans and SpringDataRepository - JPA.
 * @author diogocarleto
 *
 */
public class SpringContextUtils {
	
	/**
     * Declare the application context to be used - a Java annotation based application context requiring no XML.
     */
    final private static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

    public void registerContext() {
        ctx.register(SpringDataJpaConfiguration.class);
        ctx.scan("models");
        ctx.refresh();
        ctx.start();
    }
    
    public void deregisterContext() {
    	ctx.stop();
    }
    
    /**
     * Get Spring managedBean.
     * @param tClass
     * @return
     */
	public static <T> T getSpringBean(Class<T> tClass) {
		T bean = ctx.getBean(tClass);
		Logger.info("SpringBean:" + bean);
    	return bean;
    }
}
