name := """play-java"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  	cache,
  	javaWs,
	javaCore,
	javaJpa,
	"org.springframework" % "spring-context" % "4.1.6.RELEASE",
	"javax.inject" % "javax.inject" % "1",
	"org.springframework.data" % "spring-data-jpa" % "1.8.0.RELEASE",
	"org.springframework" % "spring-expression" % "4.1.6.RELEASE",
	"org.hibernate" % "hibernate-entitymanager" % "3.6.10.Final",
	"org.mockito" % "mockito-core" % "1.9.5" % "test"
)

libraryDependencies += filters

fork in run := true